conf t

int f0/0 
ip add dhcp
no sh
exit

int f0/1
ip add 192.168.2.1 255.255.255.0
no sh
exit



crypto isakmp policy 1
encr aes
hash md5
authentication pre-share
group 2
exit

crypto isakmp key NETWORKLESSONS address 0.0.0.0
crypto ipsec transform-set MYSET esp-aes esp-md5-hmac
mode transport
exit

crypto ipsec profile MGRE
set security-association lifetime seconds 86400
set transform-set MYSET
exit

interface tunnel10
ip add 10.0.0.2 255.255.255.0
ip mtu 1400
ip nhrp authentication donttell
ip nhrp map 10.0.0.1 192.168.122.1
ip nhrp map multicast 192.168.122.1
ip nhrp nhs 10.0.0.1
ip nhrp network-id 99
tunnel source f0/0
tunnel key 10
tunnel mode gre multipoint
tunnel protection ipsec profile MGRE
bandwidth 1000
ip tcp adjust-mss 1360
ip nhrp holdtime 450
delay 1000
no ip split-horizon 
ip ospf network point-to-multipoint
exit

router ospf 1
network 10.0.0.0 0.0.0.255 area 0
network 192.168.0.0 0.0.255.255 area 0
exit

exit

debug nhrp packet






