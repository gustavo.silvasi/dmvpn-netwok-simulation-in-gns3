conf t

int f0/0 
ip add 192.168.121.1 255.255.255.0 
no sh
exit

int f0/1
ip add 192.168.1.1 255.255.255.0
no sh
exit

ip route 0.0.0.0 0.0.0.0 192.168.121.76

interface tunnel10
ip add 10.0.0.1 255.255.255.0
ip mtu 1400
ip nhrp authentication donttell
ip nhrp map multicast dynamic
ip nhrp network-id 99
tunnel source f0/0
tunnel key 10
tunnel mode gre multipoint
bandwidth 1000
ip tcp adjust-mss 1360
ip nhrp holdtime 450
delay 1000
exit

exit




