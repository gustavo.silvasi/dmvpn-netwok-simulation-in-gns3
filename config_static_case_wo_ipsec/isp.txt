conf t

int f0/0
desc ROUTE TO HUB(R1)
ip add 192.168.121.76 255.255.255.0
no sh
exit

int f0/1
desc ROUTE TO SPOKE(R2)
ip add 192.168.122.76 255.255.255.0
no sh
exit

int f1/0
desc ROUTE TO SPOKE(R3)
ip add 192.168.123.76 255.255.255.0
no sh
exit

int f2/0
desc ROUTE TO SPOKE(R4)
ip add 192.168.124.76 255.255.255.0
no sh
exit

exit
