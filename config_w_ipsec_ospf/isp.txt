conf t

ip dhcp pool mypool2
network 192.168.122.0 /24
default-router 192.168.122.76
dns-server 8.8.8.8 8.8.4.4
exit

ip dhcp pool mypool3
network 192.168.123.0 /24
default-router 192.168.123.76
dns-server 8.8.8.8 8.8.4.4
exit

ip dhcp pool mypool4
network 192.168.124.0 /24
default-router 192.168.124.76
dns-server 8.8.8.8 8.8.4.4
exit

int f0/0
desc ROUTE TO HUB(R1)
ip add 192.168.121.76 255.255.255.0
no sh
exit

int f0/1
desc ROUTE TO SPOKE(R2)
ip add 192.168.122.76 255.255.255.0
no sh
exit

int f1/0
desc ROUTE TO SPOKE(R3)
ip add 192.168.123.76 255.255.255.0
no sh
exit

int f2/0
desc ROUTE TO SPOKE(R4)
ip add 192.168.124.76 255.255.255.0
no sh
exit

exit
